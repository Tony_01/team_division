import pandas as pd
import random
data=pd.read_csv('Sorted.csv')
list1=list(data['0'])
list2=list(data['1'])
list3=list(data['2'])
list4=list(data['3'])
final=list(zip(list1,list2,list3,list4)) #sorted list
android=[]
web=[]
others=[]
for i in final:
    if(i[3]=='Data Science'):
        android.append(i)
    elif(i[3]=='Web Development'):
        web.append(i)
    else:
        others.append(i)
male_an=[]
female_an=[]
for i in android:
    if(i[1]=='Male'):
        male_an.append(i)
    else:
        female_an.append(i)
male_web=[]
female_web=[]
for i in web:
    if(i[1]=='Male'):
        male_web.append(i)
    else:
        female_web.append(i)
male_o=[]
female_o=[]
for i in others:
    if(i[1]=='Male'):
        male_o.append(i)
    else:
        female_o.append(i)
teams=[]
while(len(android)!=0):
    if(len(male_an)<=3 or len(female_an)<=3 or len(others)<=4):
        break
    else:
        ran=random.sample(range(0,(len(male_an)-1)),3)
        ran2=random.sample(range(0,(len(female_an)-1)),3)
        ran3=random.sample(range(0,(len(others)-1)),4)
        ran.sort(reverse=True)
        ran2.sort(reverse=True)
        ran3.sort(reverse=True)
        a=[]
        b=[]
        c=[]
        temp=[]
        for i in ran:
            temp.append(male_an[i][0])
        for j in ran2:
            temp.append(female_an[j][0])
        for k in ran3:
            temp.append(others[k][0])
        items=tuple(temp)
        teams.append(items)
        for xx in ran:
            aa=male_an[xx]
            male_an.remove(aa)
            android.remove(aa)
        for yy in ran2:
            bb=female_an[yy]
            female_an.remove(bb)
            android.remove(bb)
        for zz in  ran3:
            cc=others[zz]
while(len(web)!=0):
    if(len(male_web)<=3 or len(female_web)<=3 or len(others)<=4):
        if(len(others)>=3):
            ranx=random.sample(range(0,(len(male_web)-1)),4)
            ranx2=random.sample(range(0,(len(female_web)-1)),3)
            ranx3=random.sample(range(0,(len(others)-1)),3)
            ranx.sort(reverse=True)
            ranx2.sort(reverse=True)
            ranx3.sort(reverse=True)
            temp=[]
            for ii in ranx:
                temp.append(male_web[ii][0])
            for jj in ranx2:
                temp.append(female_web[jj][0])
            for kk in ranx3:
                temp.append(others[kk][0])
            items=tuple(temp)
            teams.append(items)
            teams.append(items1)
            for xxx in ranx:
                aaa=male_web[xxx]
                male_web.remove(aaa)
                web.remove(aaa)
            for yyy in ranx2:
                bbb=female_web[yyy]
                female_web.remove(bbb)
                web.remove(bbb)
            for zzz in  ranx3:
                ccc=others[zzz]
                others.remove(ccc)
        else:
            if (len(male_web)<=5 or len(female_web)<=5):
                break
            ranx=random.sample(range(0,(len(male_web)-1)),5)
            ranx2=random.sample(range(0,(len(female_web)-1)),5)
            ranx.sort(reverse=True)
            ranx2.sort(reverse=True)
            temp=[]
            for ii in ranx:
                temp.append(male_web[ii][0])
            for jj in ranx2:
                temp.append(female_web[jj][0])
            items=tuple(temp)
            teams.append(items)
            teams.append(items1)
            for xxx in ranx:
                aaa=male_web[xxx]
                male_web.remove(aaa)
                web.remove(aaa)
            for yyy in ranx2:
                bbb=female_web[yyy]
                female_web.remove(bbb)
                web.remove(bbb)
    else:
        ranx=random.sample(range(0,(len(male_web)-1)),3)
        ranx2=random.sample(range(0,(len(female_web)-1)),3)
        ranx3=random.sample(range(0,(len(others)-1)),4)
        ranx.sort(reverse=True)
        ranx2.sort(reverse=True)
        ranx3.sort(reverse=True)
        temp=[]
        for ii in ranx:
            temp.append(male_web[ii][0])
        for jj in ranx2:
            temp.append(female_web[jj][0])
        for kk in ranx3:
            temp.append(others[kk][0])
        items1=tuple(temp)
        teams.append(items1)

        for xxx in ranx:
            male_web.remove(aaa)
            web.remove(aaa)
        for yyy in ranx2:
            bbb=female_web[yyy]
            female_web.remove(bbb)
            web.remove(bbb)
        for zzz in  ranx3:
            ccc=others[zzz]
            others.remove(ccc)
remain=[]
for s in android:
    remain.append(s)
for t in web:
    remain.append(t)
for u in others:
    remain.append(u)
fin=[]
while(len(remain)>=10):
    temp1=[]
    text_ran=random.sample(remain,10)
    for m in text_ran:
        temp1.append(m[0])
        remain.remove(m)
    tuple(temp1)
    teams.append(temp1)
if (len(remain)!=0):
    temp2=[]
    for i  in remain:
        temp2.append(i[0])
    tuple(temp2)
    print(i)
    teams.append(temp2)
dframe = pd.DataFrame(teams)
dframe.to_csv('teams.csv')
